# Swift Best Practices Report
## Ashton Lye

<br>

Apple has plenty of documentation on the 'Best Practices' when creating apps with Swift - here are some of them:

# 1. Formatting Content
>*"Create a layout that fits the screen of an iOS device. Users should see primary content without zooming or scrolling horizontally."* - Apple's UI Design Do's and Dont's

In the past you have probably used a poorly designed app or mobile site that required you to zoom in or scroll around in order to be able to see the content properly - reddit's old mobile site springs to mind immediately for me. The page content didn't constrain itself to the screen size so to read the whole post title you had to scroll horizontally across the page. Very frustrating! The best practice for Swift (and general app development) is to create a layout that ensures your main content fits in one screen. I have followed this in my app by making sure my content is narrow enough to fit on the one screen.

<br>

# 2. Touch Controls
>"*Use UI elements that are designed for touch gestures to make interaction with your app feel easy and natural.*" - Apple's UI Design Do's and Dont's

Swift has several gesture and touch control handlers built in, so you don't have to create them yourself. Apple recommends using these handlers as ones you create yourself may not fit Apple's design style and probably won't work and flow as naturally as Apple's own. I have definitely followed this in my app as I have absolutely no idea how to make my own touch controls or handlers (yet).

<br>

# 3. Hit Targets
>"*Create controls that measure at least 44 points x 44 points so they can be accurately tapped with a finger.*" - Apple's UI Design Do's and Dont's

Tapping on objects is a pretty fundamental part of navigation on mobile devices. Because of this, Apple recommends you make tappable objects at least 44 x 44 points in size so that they're big enough to be tapped easily. While you can zoom in on smaller buttons, it makes things harder than necessary and can be very frustrating to the user. I made sure that all the controls in my app were plenty big enough to be tapped easily.

<br>

# 4. Text Size
>"*Text should be at least 11 points so it's legible at a typical viewing distance without zooming.*" - Apple's UI Design Do's and Dont's

If you've got text in an app that the user has to read, you need to make sure it's big enough to be read easily. As with the Hit Targets, the user can zoom in if it's too small, but this is an extra and unnecessary step. Apple recommends font size 11 or higher for easy reading. If possible, text size should be set to the system default, as this will let it be affected by the user's text scaling setting in the system settings. I have followed this in my app by ensuring all the text is at least a few sizes bigger than 11.

<br>

# 5. Contrast
>"*Make sure there is ample contrast between the font color and the background so text is legible.*" - Apple's UI Design Do's and Dont's

Once again, when your app is using text, you must ensure that the background colour is not too similar to the text colour so your text is easy to read. For example, light coloured text + light coloured background = hard to read, but dark text on a light background is ok. In my app I took extra steps to ensure I was following this practice - I made a function to change the text colour based on how dark the background is to ensure the text was still readable against the background. 

<br>

# 6. Spacing
>"*Don't let text overlap. Improve legibility by increasing line height or letter spacing.*" - Apple's UI Design Do's and Dont's

Another point to do with text is line and character spacing - if the lines are too close together the text can be hard to read, but too far apart makes it looks bad and can waste a lot of screen space. If the text is overlapping it can be borderline unreadable! I followed this in my app by making sure all of my labels were far enough apart that they weren't difficult to read.

<br>

# 7. Organization
>"*Create an easy-to-read layout that puts controls close to the content they modify.*" - Apple's UI Design Do's and Dont's

Apple recommends that all your controls are put in a logical layout - controls should be close to the content they modify. Similar to this, controls should be labelled or laid out in a manner that makes it easy to understand what each control does.

<br>

# 8. Alignment
>"*Align text, images, and buttons to show users how information is related.*" - Apple's UI Design Do's and Dont's

Similar to Organization, Apple recommends that UI elements are aligned to show the relationship between the information on the screen. For example, image captions should be aligned with the image to show clearly that the caption is referring to the image. Additionally, as we mentioned in Organization, controls should also be placed in a manner that makes it clear what the control relates to. I have followed this in my app by making sure the sliders and buttons are clearly identified by labels near or on the control.

<br>

# Sources
Apple's UI Design Do's and Dont's - https://developer.apple.com/design/tips/

Apple's Human Interface Guidelines - https://developer.apple.com/ios/human-interface-guidelines/overview/design-principles/

<br>

# Peer Reviewing
I find myself agreeing with the best practices that **Ashton Lye** has put forward. They seem well researched in terms of their relevance, and it's been suscinctly written in this report. My understanding of best practices is coming from my background in *Xamarin*, another mobile application development service. I believe that work done in this report deserves a full amount of the marks available for this section.
> "Good stuff" - James