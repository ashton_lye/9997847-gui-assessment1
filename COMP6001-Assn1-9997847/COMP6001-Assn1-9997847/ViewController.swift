//
//  ViewController.swift
//  COMP6001-Assn1-9997847
//
//  Created by Ashton Lye on 9/08/17.
//  Copyright © 2017 Ashton Lye. All rights reserved.
//
// COMP6001 Assessment 1 - Ashton Lye

import UIKit

class ViewController: UIViewController {
    
    //Outlet collections so I can loop through to change all labels/buttons/sliders at once rather than individually
    @IBOutlet var textLabels: [UILabel]!
    @IBOutlet var buttons: [UIButton]!
    @IBOutlet var sliders: [UISlider]!
    
    //Outlets for sliders & their labels, declaring vars for slider values
    @IBOutlet weak var redLabel: UILabel!
    @IBOutlet weak var redSlider: UISlider!
    var redValue:Float = 0.5
    
    @IBOutlet weak var greenLabel: UILabel!
    @IBOutlet weak var greenSlider: UISlider!
    var greenValue:Float = 0.5
    
    @IBOutlet weak var blueLabel: UILabel!
    @IBOutlet weak var blueSlider: UISlider!
    var blueValue:Float = 0.5
    
    var Alpha = 1
    
    //Creating the UIColor from the values the user provides
    var userColour: UIColor {
        get {
            return UIColor(red: CGFloat(redValue), green: CGFloat(greenValue), blue: CGFloat(blueValue), alpha: CGFloat(Alpha))
        }
    }
    
    //Actions for when the sliders are slid - changing label and assigning value
    @IBAction func redSliderValueChanged(_ sender: Any) {
        redValue = redSlider.value
        redLabel.text = ("Red Value: \((redValue * 255).rounded())")
    }
    
    @IBAction func greenSliderValueChanged(_ sender: Any) {
        greenValue = greenSlider.value
        greenLabel.text = ("Green Value: \((greenValue * 255).rounded())")
    }
    
    @IBAction func blueSliderValueChanged(_ sender: Any) {
        blueValue = blueSlider.value
        blueLabel.text = ("Blue Value: \((blueValue * 255).rounded())")
    }
    
    //Actions for when Set and Reset buttons are pressed - updateUI function is used for both
    @IBAction func setColourButtonPressed(_ sender: Any) {
        updateUI(updateType: "set")
    }
    
    @IBAction func resetButtonPressed(_ sender: Any) {
        updateUI(updateType: "reset")
    }
    
    //updateUI function for updating background/text colour, slider pos etc
    func updateUI(updateType :String) {
        let valueSum = (redValue + greenValue + blueValue) * 255
        
        //updateType == reset when reset button pressed & when app first starts
        if updateType == "reset" {
            view.backgroundColor = .white
            
            redValue = 0.5
            redLabel.text = "Red Value"
            greenValue = 0.5
            greenLabel.text = "Green Value"
            blueValue = 0.5
            blueLabel.text = "Blue Value"
            
            //this is why outlet collections - rather than having to reference each slider/label/button individually I can just loop through them in the collection
            for slider in sliders {
                slider.value = 0.5
            }
            
            for label in textLabels {
                label.textColor = .black
            }
            
            for button in buttons {
                button.tintColor = .black
                button.layer.borderWidth = 1
                button.layer.borderColor = UIColor.black.cgColor
                button.layer.cornerRadius = 5
            }
            
        //normal update - when set background button is pressed
        } else {
            view.backgroundColor = userColour
            
            //changing the text colour on labels/buttons when the sum of the colour values is less than 300 - lazy way of checking if the background is too dark for black to be read easily
            for label in textLabels {
                if valueSum < 300 && greenValue < 0.83 && redValue < 0.83 {
                    label.textColor = .white
                }
                else {
                    label.textColor = .black
                }
            }
            
            for button in buttons {

                if valueSum < 300 && greenValue < 0.83 && redValue < 0.83 {
                    button.tintColor = .white
                    button.layer.borderColor = UIColor.white.cgColor
                }
                else {
                    button.tintColor = .black
                    button.layer.borderColor = UIColor.black.cgColor
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI(updateType: "reset")
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

